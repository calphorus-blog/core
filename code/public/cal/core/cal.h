/* Copyright (c) 2019 Anthony Smith
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */
#if !defined( CAL_H )
#    define CAL_H 1

#    if __cplusplus
// This is C++
#        define CAL_CXX 1
#    else // __cplusplus
#        define CAL_CXX 0
#    endif // __cplusplus

// 64-bit detection is different for different compilers. The compilers to be
// supported are: MSVC, Clang, and GCC.
#    if _MSC_VER
#        if defined( WIN64 ) || defined( _WIN64 )
#            define CAL_64BIT 1
#        else // defined( WIN64 ) || defined( _WIN64 )
#            define CAL_64BIT 0
#        endif // defined( WIN64 ) || defined( _WIN64 )
#    elif __clang__ // _MSC_VER
#        if defined( __LP64__ ) && __LP64__
#            define CAL_64BIT 1
#        else // defined( __LP64__ ) && __LP64__
#            define CAL_64BIT 0
#        endif // defined( __LP64__ ) && __LP64__
#    elif __GNUC__ // _MSC_VER
#        if defined( __LP64__ ) && __LP64__
#            define CAL_64BIT 1
#        else // defined( __LP64__ ) && __LP64__
#            define CAL_64BIT 0
#        endif // defined( __LP64__ ) && __LP64__
#    endif // _MSC_VER

#    if CAL_CXX
// For the basic integer types.
#        include <cstdint>
#    else // CAL_CXX
// For the basic integer types. Added to C in C99.
#        include <stdint.h> 
// This adds bool, true, and false to C. Added to C in C99.
#        include <stdbool.h>
#    endif // CAL_CXX

#    if CAL_CXX
namespace cal
{
#    endif // CAL_CXX

typedef int8_t   int8;
typedef int16_t  int16;
typedef int32_t  int32;
typedef int64_t  int64;
typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
    
    // Pretty sure this is the case for most platforms:
typedef float    float32;
typedef double   float64;

#    if CAL_64BIT
typedef int64 intptr;
#    else // CAL_64BIT
typedef int32 intptr;
#    endif // CAL_64BIT

// The character type used on Windows will be different than the one used in
// any other operating system.
// On Windows, _WIN32 or WIN32 is always defined, regardless of if it is 32-bit
// or not.
#    if defined( WIN32 ) || defined( _WIN32 )
typedef wchar_t cchar;
#        define CAL_STR( Text ) L ## Text
#    else // defined( WIN32 ) || defined( _WIN32 )
typedef char    cchar;
#        define CAL_STR( Text ) Text
#    endif // defined( WIN32 ) || defined( _WIN32 )

#    if CAL_CXX
} // namespace cal
#    endif // CAL_CXX

#endif // !defined( CAL_H )